<!-- # 最常用的MySQL命令 -->

## 登录命令

```mysql
mysql -uroot -proot
mysql -uroot -p
mysql -uroot -p
```

## 创建数据库

```mysql
create databases 数据库名;
```

## 查看所有数据库

```mysql
show databases;
```

## 选中数据库

```mysql
use 数据库名;
```

## 删除数据库

```mysql
drop database 数据库名;
```

## 查看数据库的表

```mysql
show tables;
```

## 创建数据库

```mysql
create database 数据库名称;
```

## 创建数据库中的表

```mysql
create table 表名(        
	字段1名称 int not null auto_increment,
    字段2名称 varchar(100) not null,
    字段3名称 varchar(40) not null,
    字段4名称 date,
    primary key(字段 名称)
)engine=myisam default cahrset=utf8;
```

## 查询数据库中表的数据

```mysql
select * from 表名;
```

## 向数据库中插入数据

```mysql
insert into 表名 (字段1名,字段2名 .....) valves(字段1值,字段2值 ....);
```

## 修改数据库中某一条数据

```mysql
update 表名 set 字段名=值,字段名=值,... where id=2;
```

## 删除数据库中的某一条数据

```mysql
delete from 表名 where 字段名=值;
```

## 删除一个数据库中的一个表

```mysql
drop table 表名;
```

## 查询一个表的表结构

```mysql
desc 表名;
```

## 向一个数据库中的一个表新增一个字段

```mysql
alter table 表名 add 字段名 类型 not null;
```

## 把一个数据库中的一个表的一个字段属性进行修改

```mysql
alter table 表名 alter column 字段名 类型 not null;
```

## 字段修改

```mysql
alter table 表名 alter change 修改前的字段名 修改后的字段名 类型 not null;
```

## 把一个数据库中的一个表的一个字段删除

```mysql
alter table 表名 drop column 字段名;
```
