### 说一下 spring mvc 运行流程？

- spring mvc 先将请求发送给 DispatcherServlet。

- DispatcherServlet 查询一个或多个 HandlerMapping，找到处理请求的 Controller。

- DispatcherServlet 再把请求提交到对应的 Controller。

- Controller 进行业务逻辑处理后，会返回一个ModelAndView。

- Dispathcher 查询一个或多个 ViewResolver 视图解析器，找到 ModelAndView 对象指定的视图对象。

- 视图对象负责渲染返回给客户端。

### spring mvc 有哪些组件？

- 前置控制器 DispatcherServlet。

- 映射控制器 HandlerMapping。

- 处理器 Controller。

- 模型和视图 ModelAndView。

- 视图解析器 ViewResolver。

### @RequestMapping 的作用是什么？

将 http 请求映射到相应的类/方法上。

103、@Autowired 的作用是什么？

@Autowired 它可以对类成员变量、方法及构造函数进行标注，完成自动装配的工作，通过@Autowired 的使用来消除 set/get 方法。
